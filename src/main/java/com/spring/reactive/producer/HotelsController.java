package com.spring.reactive.producer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping(value = "/hotels")
public class HotelsController {
    @Autowired
    private HotelRepository hotelRepository;

    @GetMapping
    public List<Hotel> getHotels() {
        //TODO: CODE FOR FETCHING THE HOTELS FROM H2 DB
        return fetchHotels();
    }

    private List<Hotel> fetchHotels() {
        List<Hotel> hotels = new ArrayList<>();
        hotelRepository.findAll().forEach(hotelEntity -> {
            hotels.add(new Hotel(hotelEntity.id, hotelEntity.name));
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        });

        return hotels;
    }

    @GetMapping(value = "/stream")
    public Flux<Hotel> getHotelsStream(){
        return Flux.fromIterable(hotelRepository.findAll())
                .delayElements(Duration.ofSeconds(1)).map(
                        hotelEntity -> new Hotel(hotelEntity.id, hotelEntity.name)
                );
    }

}
